## Ignition delay time sensitivity (IDS) calculator

A Python code to estimate octane numbers and ignition delay time sensitivity based on the methodology described in [1] is provided. The code is compatible with both versions of Python, 2.x and 3.x.

**References**

[1] Naser, N., Sarathy, S. M., & Chung, S. H. (2018). Ignition delay time sensitivity in ignition quality tester (IQT) and its relation to octane sensitivity. Fuel, 233, 412-419. doi:[10.1016/j.fuel.2018.05.131](https://doi.org/10.1016/j.fuel.2018.05.131 "10.1016/j.fuel.2018.05.131").