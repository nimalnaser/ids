#====================================================================================
# Ignition delay time sensitivity calculator
#====================================================================================
#
# Author            : Nimal Naser (nimal.naser@kaust.edu.sa, nimal.naser@gmail.com)
# Institute         : King Abdullah University of Science and Technology (KAUST)
# Department        : Mechanical Engineering
# Research center   : Clean Combustion Research Center
# Version           : 1.0.0
# Created on        : 11 January 2018
# Last modified on  : 12 August 2018
#
#------------------------------------------------------------------------------------
#
#------------------------------------------------------------------------------------


#------------------------------------------------------------------------------------
# Import module
#------------------------------------------------------------------------------------
from __future__ import division

#------------------------------------------------------------------------------------
# Begin timer
#------------------------------------------------------------------------------------
import timeit
time_start = timeit.default_timer()

#------------------------------------------------------------------------------------
# Compatibility for Python 2.x and 3.x
#------------------------------------------------------------------------------------
try:
    input = raw_input
except NameError:
    pass

#------------------------------------------------------------------------------------
# Import modules and functions
#------------------------------------------------------------------------------------
import os
import logging
import math
import time
import sys

#------------------------------------------------------------------------------------
# Log file
#------------------------------------------------------------------------------------
if not os.path.isdir('./Logs'):
    os.makedirs('./Logs')
logging.basicConfig(filename = './Logs/IDS - Log - ' + \
    str(time.strftime('%Y-%m-%d %H:%M:%S')) + '.log', level = logging.DEBUG, \
    filemode = 'w', format = '%(asctime)s\t%(levelname)s\t%(message)s\t', \
    datefmt = '[%Y-%m-%d %H:%M:%S]')
logging.info(u'\t\u2714\t\u229D Running code.')

#------------------------------------------------------------------------------------
# Obtain ignition delay times
#------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------
# Obtain ignition delay time at DCN condition
#------------------------------------------------------------------------------------
restart='y'
while restart not in ('n'):
    logging.info(u'\t\u2714\t\t\u25CF Obtain ignition delay time at DCN condition.')
    while True:
        idt_dcn = input('Enter ignition delay time ' + \
            'at DCN condition (in ms): ')
        try:
            val = float(idt_dcn)
            logging.info(u'\t\u2714\t\t\t\u25C6 Ignition delay time at DCN ' + \
                'condition a numerical value.')
            if float(idt_dcn) <= 0:
                print 'Please enter a positive value for ignition delay time ' + \
                    'at DCN condition'
                logging.warning(u'\t\u2716\t\t\t\t\u27A4 Ignition delay time ' + \
                    'at DCN condition not a positive value.')
            else:
                logging.info(u'\t\u2714\t\t\t\t\u27A4 Ignition delay time ' + \
                    'input at DCN condition a positive value.')
                break                
        except ValueError:
            print 'Please enter a numerical value.'
            logging.warning(u'\t\u2716\t\t\t\u25C6 Ignition delay time at ' + \
                'DCN condition not a numerical value.')
    idt_dcn = float(idt_dcn)
    logging.info(u'\t\u2714\t\t\u25CF Obtained ignition delay time at DCN ' + \
        'condition.')

#------------------------------------------------------------------------------------
# Obtain ignition delay time at IDS condition
#------------------------------------------------------------------------------------
    logging.info(u'\t\u2714\t\t\u25CF Obtain ignition delay time at IDS condition.')
    while True:
        idt_l = input('Enter ignition delay time ' + \
            'at IDS condition (in ms): ')
        try:
            val = float(idt_l)
            logging.info(u'\t\u2714\t\t\t\u25C6 Ignition delay time at IDS ' + \
                'condition a numerical value.')
            if float(idt_l) <= 0:
                print 'Please enter a positive value for ignition delay time ' + \
                    'at IDS condition'
                logging.warning(u'\t\u2716\t\t\t\t\u27A4 Ignition delay time ' + \
                    'at IDS condition not a positive value.')
            else:
                logging.info(u'\t\u2714\t\t\t\t\u27A4 Ignition delay time ' + \
                    'input at IDS condition a positive value.')
                break 
        except ValueError:
            print 'Please enter a numerical value.'
            logging.warning(u'\t\u2716\t\t\t\u25C6 Ignition delay time at ' + \
                'IDS condition not a numerical value.')
    idt_l = float(idt_l)
    logging.info(u'\t\u2714\t\t\u25CF Obtained ignition delay time at IDS ' + \
        'condition.')

#------------------------------------------------------------------------------------
# Calculate DCN
#------------------------------------------------------------------------------------
    if 3.1 <= idt_dcn <= 6.5:
        dcn = 4.46 + (186.6 / idt_dcn)
    else:
        dcn = 83.99 * ((idt_dcn - 1.512) ** -0.658) + 3.547

#------------------------------------------------------------------------------------
# Calculate IDT_l of PRF at IDS condition
#------------------------------------------------------------------------------------
    idt_l_prf = 131 * (idt_dcn / 100) - (13 / idt_dcn) + 6.0
    
#------------------------------------------------------------------------------------
# Calculate IDS, OS, predicted RON and MON
#------------------------------------------------------------------------------------
    ids = idt_l / idt_l_prf
    os = (34 * math.log(ids)) / (0.84 + math.log(ids))
    ron = - 293 * ((dcn / 100) ** 2) - 52 * (dcn /100) + 114.1
    mon = ron - os

#------------------------------------------------------------------------------------
# Display output
#------------------------------------------------------------------------------------
    print '---------\n Results\n---------'
    print 'Predicted RON: ' + str(round(ron,1))
    print 'Predicted MON: ' + str(round(mon,1))
    print '-------------------\n Additonal results\n-------------------'    
    print 'DCN: ' + str(round(dcn,1))
    print 'IDT of PRF at IDS condition : ' + str(round(idt_l_prf,1))
    print 'IDS: ' + str(round(ids,3))

#------------------------------------------------------------------------------------
# Run code again
#------------------------------------------------------------------------------------
    restart = input('Run code again (y/n)?: ').lower()
    while restart not in ('y','n'):
        logging.warning(u'\t\u2716\t\t\t\t\u27A4 Wrong input provided.')
        sys.stdout.write('Please respond with ''Y/y'' or ''N/n''\n')
        restart = input('Run code again (y/n)?: ').lower()
    if restart == 'y':
        logging.info(u'\t\u2714\t\u229D Running code again.')
        print '====================\n Running code again\n===================='
    elif restart == 'n':
        print 'Exiting'
logging.info(u'\t\u2714\t\t\u263B Normal exit')
time_end = timeit.default_timer()
logging.info(u'\t\u2714\t\t\u231B Finished in ' + \
    str(round(time_end - time_start, 2)) + ' s')